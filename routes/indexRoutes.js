const authRoutes = require("./authRoutes");

const routes = [authRoutes];

const configureRoutes = app => {
  routes.forEach(route => route(app));
};

module.exports = configureRoutes;
