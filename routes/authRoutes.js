const configureAuthRoutes = app => {
  // login
  app.post("/api/v1/auth/login", (req, res, next) => {
    var cookie = req.cookies.taskManagerAuth;
    if (cookie == undefined) {
      res.cookie("taskManagerAuth", req.body.username, {
        httpOnly: true,
        maxAge: 9000000000
      });
      res.send("Cookies were setted");
    } else {
      res.send("You have already logged in!");
    }
    next();
  });

  // logout
  app.post("/api/v1/auth/logout", (req, res, next) => {
    res.clearCookie("taskManagerAuth");
    res.sendStatus(200);
  })
};

module.exports = configureAuthRoutes;
