const express = require("express");
const bodyParser = require('body-parser');
const cookieParser = require("cookie-parser");
const cors = require('cors');

const configureRoutes = require("./routes/indexRoutes")

const app = express();
const port = 4200;

app.use(cors({
  origin: [
    'http://localhost:3000',
    'https://localhost:3000'
  ],
  credentials: true,
  exposedHeaders: ['set-cookie']
}));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());

configureRoutes(app);

const server = app.listen(port, () =>
  console.log(`Listening at http://localhost:${port}`)
);

module.exports.close = () => {
  server.close();
};